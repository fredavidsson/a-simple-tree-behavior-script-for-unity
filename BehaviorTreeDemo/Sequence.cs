
namespace LearningAI
{
    public class Sequence : Node
    {
        public Sequence(string n) 
        {
            name = n;
        }
        public override Status Process()
        {
            Status childStatus = children[currentChild].Process();
            

            if(childStatus == Status.RUNNING)
            {
                return Status.RUNNING;
            }

            if(childStatus == Status.FAILURE)
            {
                // Debug.Log("child " + children[currentChild].name + " failed");
                return childStatus;
            }

            
            currentChild++;

            // Reset sequence
            if(currentChild >= children.Count)
            {
                // Debug.Log("Reset sequence");
                currentChild = 0;
            }

            return Status.RUNNING;
        }
    }
}

