using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace LearningAI
{
    public class BTAgent : MonoBehaviour
    {
        public Transform back;

        protected BehaviorTree tree;
        
        protected NavMeshAgent agent;

        WaitForSeconds waitForSeconds;

        public enum ActionState { IDLE, MOVING };
        ActionState state = ActionState.IDLE;

        Node.Status treeStatus = Node.Status.RUNNING;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        protected void Awake()
        {
            agent = this.GetComponent<NavMeshAgent>();
            tree = new BehaviorTree();
            waitForSeconds = new WaitForSeconds(Random.Range(0.1f, 1f));
        }

        protected void Start()
        {
            StartCoroutine("Behave");
        }

        // Start is called before the first frame update
        public Node.Status GoToDoor(Transform door)
        {
            Node.Status s = GoToLocation(door.position);

            // Agent made it to the door
            if(s == Node.Status.SUCCESS)
            {
                if(!door.GetComponent<Lock>().isLocked)
                {
                    // door.gameObject.SetActive(false);
                    // door.GetComponent<NavMeshObstacle>().enabled = false;
                    door.GetComponent<Door>().Open();

                    return Node.Status.SUCCESS;
                }

                return Node.Status.FAILURE;
            }
            else
            {
                return s;
            }
        }

        public Node.Status GoToLocation(Vector3 destination)
        {
            float distance = Vector3.Distance(destination, this.transform.position);
            if(state == ActionState.IDLE)
            {
                agent.SetDestination(destination);
                // Debug.Log("Going to a new destination");
                state = ActionState.MOVING;
            }
            else if (Vector3.Distance(agent.pathEndPosition, destination) >= 1f)
            {
                state = ActionState.IDLE;
                Debug.Log("Unable to reach destination");
                return Node.Status.FAILURE;
            }
            else if (distance < 1f)
            {
                // Debug.Log("Reached the destination");
                state = ActionState.IDLE;
                return Node.Status.SUCCESS;
            }

            return Node.Status.RUNNING;
        }

        IEnumerator Behave()
        {
            while(true)
            {
                treeStatus = tree.Process();
                yield return waitForSeconds;
            }
        }

        /*
        void Update()
        {
            if(treeStatus != Node.Status.SUCCESS)
            {
                treeStatus = tree.Process();
            }
        }
        */

    }
}


