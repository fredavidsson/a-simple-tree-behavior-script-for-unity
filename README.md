# My tree-behavior demo

These are some of the files used in my demo project.
Here's a video: [https://youtu.be/xwEf0ymOyhs](https://youtu.be/xwEf0ymOyhs) to see the demo running. 

The green (the thief) character uses the file BTThiefNPC.cs, white uses BTDeliveryNPC.cs and the blue character uses the NPCBehaviorTest.cs file. One might wanna start looking at these files first together with the video above. 

These NPCs are moving around using the standard navigation mesh (NavMesh) that comes with Unity and are trying to reach their goal by using the NavMeshAgent, see BTAgent.cs on how it is used. 
