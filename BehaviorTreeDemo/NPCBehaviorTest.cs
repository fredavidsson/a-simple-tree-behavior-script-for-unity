using UnityEngine;

namespace LearningAI
{
    public class NPCBehaviorTest : BTAgent
    {

        public GameObject homeDestination, outsideDestination;

        public GameObject PickableItem;

        public Transform[] otherDestinations;

        public Transform[] doors;

        [Range(0, 500)]
        public float resource = 25f;

        new void Awake()
        {
            base.Awake();
        }
        // Start is called before the first frame update
        new void Start()
        {
            base.Start();
            tree = new BehaviorTree();
            Sequence walk = new Sequence("Walk");
            Leaf goToOutside = new Leaf("To destination", GoToOutside);
            Leaf goToHome = new Leaf("To home", GoToHome);

            Leaf goToSouthDoor = new Leaf("To south door", GoToSouthDoor);
            Leaf goToNorthDoor = new Leaf("To north door", GoToNorthDoor);
            Leaf goToWestDoor = new Leaf("To west door", GoToWestDoor);

            Leaf hasGotResouce = new Leaf("Has got resource", HasResources);

            Leaf goToPickupObject = new Leaf("Go to pickup item", GoToPickupObject);

            Selector openDoor = new Selector("Open door");

            Inverter invertResource = new Inverter("Invert money");
            invertResource.AddChild(hasGotResouce);

            // Leaf pickObject = new Leaf("Pickup object");

            
            openDoor.AddChild(goToNorthDoor);
            openDoor.AddChild(goToWestDoor);
            openDoor.AddChild(goToSouthDoor);
            

            // walk.AddChild(hasGotResouce);
            walk.AddChild(invertResource);

            // pickObject.AddChild(goToPickupObject);

            walk.AddChild(openDoor);

            // walk.AddChild(goToOutside);
            walk.AddChild(goToPickupObject);

            walk.AddChild(goToHome);

            tree.AddChild(walk);

            tree.PrintTree();
        }

        public Node.Status GoToNorthDoor()
        {
            //agent.speed = 3.0f;
            return GoToDoor(doors[0]);
        }

        public Node.Status GoToWestDoor()
        {
            //agent.speed = 3.0f;
            return GoToDoor(doors[3]);
        }

        public Node.Status GoToSouthDoor()
        {
            //agent.speed = 3.0f;
            return GoToDoor(doors[2]);
        }
        
        public Node.Status GoToPickupObject()
        {
            Node.Status s = GoToLocation(PickableItem.transform.position);

            if(s == Node.Status.SUCCESS)
            {
                if(PickableItem.GetComponent<Pickable>().isPickable)
                {
                    PickableItem.GetComponent<Pickable>().isPickable = false;
                    PickableItem.GetComponent<Rigidbody>().useGravity = false;
                    PickableItem.transform.SetParent(back);
                    PickableItem.transform.localPosition = Vector3.zero - Vector3.forward * 0.25f;
                    PickableItem.transform.localRotation = Quaternion.identity;
                }

                return Node.Status.SUCCESS;
            }
            else
            {
                return s;
            }

        }

        public Node.Status GoToOutside()
        {
            // agent.speed = 3.0f;
            return GoToLocation(outsideDestination.transform.position);
        }

        public Node.Status GoToHome() 
        {
            //agent.speed = 1.5f;
            Node.Status s = GoToLocation(homeDestination.transform.position);

            // If we are home
            if(s == Node.Status.SUCCESS)
            {
                if(PickableItem.transform.parent != null)
                {
                    PickableItem.transform.parent = null;
                    PickableItem.GetComponent<Rigidbody>().useGravity = true;
                    PickableItem.GetComponent<Pickable>().isPickable = true;
                    resource += 100;
                }
                
            }

            return s;
        }

        public Node.Status HasResources()
        {
            if(resource < 100f)
            {
                return Node.Status.FAILURE;
            }

            return Node.Status.SUCCESS;
        }

        public Node.Status GoToNext0()
        {
            //agent.speed = 3.0f;
            return GoToLocation(otherDestinations[0].position);
        }
        public Node.Status GoToNext1()
        {
            return GoToLocation(otherDestinations[1].position);
        }
        public Node.Status GoToNext2()
        {
            return GoToLocation(otherDestinations[2].position);
        }


        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>

    }
}


