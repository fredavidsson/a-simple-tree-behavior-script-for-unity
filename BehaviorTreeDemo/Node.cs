using System.Collections.Generic;

namespace LearningAI
{
    
    public class Node
    {
        public enum Status
        {
            SUCCESS, RUNNING, FAILURE
        };
        
        public Status status;
        public List<Node> children = new List<Node>();
        public int currentChild = 0;

        public string name;
        
        public int sortOrder;

        public Node() { }

        public Node(string n)
        {
            name = n;
        }
        public Node(string n, int order)
        {
            name = n;
            sortOrder = order;
        }
        public void AddChild(Node n)
        {
            children.Add(n);
        }

        public virtual Status Process()
        {
            return children[currentChild].Process();
        }

    }

}
