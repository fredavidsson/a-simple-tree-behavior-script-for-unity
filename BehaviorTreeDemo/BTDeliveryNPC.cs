
using UnityEngine;


namespace LearningAI
{
    public class BTDeliveryNPC : BTAgent
    {

        public Transform pickUpDestination, deliveryDestination;

        [SerializeField]
        ItemScanner itemScanner;

        [SerializeField]
        Transform item;

        new void Awake()
        {
            base.Awake();
        }

        // Start is called before the first frame update
        new void Start()
        {
            base.Start();
            tree = new BehaviorTree();
            Sequence delivery = new Sequence("Delivery");
            Leaf goToDelivery = new Leaf("Go to delivery", GoToDelivery);
            Leaf goToPickup = new Leaf("Go to pickup", GoToPickup);

            Leaf boxIsAvailable = new Leaf("Has box available", BoxIsAvailable);

            Leaf pickupBox = new Leaf("Picking up box", PickupBox);


            delivery.AddChild(boxIsAvailable);

            delivery.AddChild(pickupBox);



            delivery.AddChild(goToDelivery);

            delivery.AddChild(goToPickup);

            tree.AddChild(delivery);

            tree.PrintTree();
        }

        
        public Node.Status PickupBox()
        {
            Node.Status s = GoToLocation(item.position);

            if(s == Node.Status.SUCCESS)
            {

                item.GetComponent<Pickable>().isPickable = false;
                item.GetComponent<Rigidbody>().useGravity = false;
                item.SetParent(back);
                item.localPosition = Vector3.zero - Vector3.forward * 0.25f;
                item.transform.localRotation = Quaternion.identity;


                return Node.Status.SUCCESS;
            }
            else
            {
                return s;
            }

        }

        public Node.Status GoToDelivery()
        {
            // agent.speed = 3.0f;
            Node.Status s =  GoToLocation(deliveryDestination.position);

            if(s == Node.Status.SUCCESS)
            {
                if(item != null)
                {
                    item.transform.parent = null;
                    item.GetComponent<Rigidbody>().useGravity = true;
                    item.GetComponent<Pickable>().isPickable = true;

                    item = null;
                }
            }

            return s;

        }

        public Node.Status GoToPickup() 
        {
            //agent.speed = 1.5f;
            Node.Status s = GoToLocation(pickUpDestination.position);

            if(s == Node.Status.SUCCESS)
            {
                itemScanner.SetTryFindItem(true);
                
            }

            return s;
        }

        public Node.Status BoxIsAvailable()
        {
            item = itemScanner.GetPickableItem();
            if(item == null)
            {
                return Node.Status.FAILURE;
                
            }
            itemScanner.SetTryFindItem(false);
            return Node.Status.SUCCESS;
        }

    }
}


