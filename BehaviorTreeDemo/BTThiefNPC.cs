
using UnityEngine;


namespace LearningAI
{
    public class BTThiefNPC : BTAgent
    {

        public Transform deliveryDestination;

        public Transform thiefDoor, eastDoor, northDoor;

        public GameObject painting;
        public GameObject diamond;

        private GameObject pickup;

        [Range(0f, 500f)]
        public float money = 500f;

        new void Awake()
        {
            base.Awake();
        }

        // Start is called before the first frame update
        new void Start()
        {
            base.Start();
            tree = new BehaviorTree();
            Sequence steal = new Sequence("Steal");

            
            Leaf openThiefDoor = new Leaf("Opening thief door", OpenThiefDoor);

            Selector openDoorsAtHomeSelector = new Selector("Opening one of the doors at Home");
            Leaf openNorthDoor = new Leaf("Opening north door", OpenNorthDoor);
            Leaf openEastDoor = new Leaf("Opening east door", OpenEastDoor);

            openDoorsAtHomeSelector.AddChild(openEastDoor);
            openDoorsAtHomeSelector.AddChild(openNorthDoor);
            
            // ----------------------------------------------------------------------
            // Either steal the diamond or the painting.
            int priority = 1; // steals the diamond first before the painting.
            Leaf stealPainting = new Leaf("Go and steal painting", StealPainting, priority + 1); 
            Leaf stealDiamond = new Leaf("Go and steal the diamond", StealDiamond, priority);

            PSelector stealSelector = new PSelector("Steal painting or diamond if available");
            stealSelector.AddChild(stealPainting);
            stealSelector.AddChild(stealDiamond);
            // ----------------------------------------------------------------------

            Leaf deliverObject = new Leaf("Deliver object home", DeliverObject);
            Leaf hasGotMoney = new Leaf("Has got money", HasEnoughMoney);


            Inverter invertMoney = new Inverter("Invert money");
            invertMoney.AddChild(hasGotMoney);

            
            // Do we have enough money?
            steal.AddChild(invertMoney);

            // if not, then open the door
            steal.AddChild(openThiefDoor);

            // Open one of the doors at Home
            steal.AddChild(openDoorsAtHomeSelector);

            // steal the diamond or the painting
            steal.AddChild(stealSelector);
            
            // deliver the stolen object home
            steal.AddChild(deliverObject);


            tree.AddChild(steal);
            tree.PrintTree();
        }

        private Node.Status StealDiamond()
        {
            if(!diamond.activeSelf)
            {
                return Node.Status.FAILURE;
            }
            
            Node.Status s = GoToLocation(diamond.transform.position);
            if( s == Node.Status.SUCCESS )
            {
                diamond.transform.SetParent(back);
                diamond.transform.localPosition = Vector3.zero - Vector3.forward * 0.25f;
                diamond.transform.localRotation = Quaternion.identity;

                pickup = diamond;
            }

            return s;
        }

        private Node.Status HasEnoughMoney()
        {
            if(money < 100f)
            {
                return Node.Status.FAILURE;
            }

            return Node.Status.SUCCESS;
        }

        Node.Status OpenThiefDoor()
        {
            return GoToDoor(thiefDoor);
        }

        Node.Status OpenNorthDoor()
        {
            return GoToDoor(northDoor);
        }
        
        Node.Status OpenEastDoor()
        {
            return GoToDoor(eastDoor);
        }

        public Node.Status StealPainting()
        {

            if(!painting.activeSelf)
            {
                return Node.Status.FAILURE;
            }

            Node.Status s = GoToLocation(painting.transform.position);

            if(s == Node.Status.SUCCESS)
            {

                painting.GetComponent<Pickable>().isPickable = false;
                painting.GetComponent<Rigidbody>().useGravity = false;
                painting.transform.SetParent(back);
                painting.transform.localPosition = Vector3.zero - Vector3.forward * 0.25f;
                painting.transform.localRotation = Quaternion.identity;

                pickup = painting;

                return Node.Status.SUCCESS;
            }
            else
            {
                return s;
            }

        }

        public Node.Status DeliverObject() 
        {
            //agent.speed = 1.5f;
            Node.Status s = GoToLocation(deliveryDestination.position);

            if(s == Node.Status.SUCCESS)
            {
                
                pickup.transform.parent = null;
                pickup.SetActive(false);
                money += 100f;

                /*
                if(painting.transform.parent == back)
                {
                    painting.transform.parent = null;
                    painting.SetActive(false);
                    money += 100f;
                } else if (diamond.transform.parent == back)
                {
                    diamond.transform.parent = null;
                    diamond.SetActive(false);
                    money += 300f;
                }
                */
                
            }

            return s;
        }

    }
}


